package com.mcshop.sklep.helpers;

public class AddressDataHelper {

    public String title;
    public String firstName;
    public String lastName;
    public String email;
    public String password;
    public String birthdate;
    public String address;
    public String city;
    public String postalCode;
}
