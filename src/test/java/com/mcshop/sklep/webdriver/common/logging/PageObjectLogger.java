package com.mcshop.sklep.webdriver.common.logging;

import com.mcshop.sklep.webdriver.common.configuration.Configuration;
import com.mcshop.sklep.webdriver.common.utils.test.TestUtils;

import java.util.ArrayList;
import java.util.List;

public class PageObjectLogger {
    /*-------------------------------------*
     * FIELDS DECLARATION AND DEFINITION
     *-------------------------------------*/
    private static final String BROWSER = Configuration.BROWSER.get();
    private static int testCounter = 1;
    private static ThreadLocal<List<String>> logTable = ThreadLocal.withInitial(ArrayList::new);
    private static ThreadLocal<List<String>> errorTable = ThreadLocal.withInitial(ArrayList::new);
    public enum LogType{
        INFO("INFO"),
        TEST_SUCCESS("TEST SUCCESS"),
        WARNING("WARNING"),
        TEST_FAILED("TEST FAILED"),
        ERROR("ERROR"),
        TEST_SKIPPED("TEST SKIPPED"),
        TEST_STARTED("TEST STARTED");

        private String name;

        LogType(String name){
            this.name = name;
        }
    }

    /*-------------------------------------*
     * METHODS DEFINITION
     *-------------------------------------*/
    /*------------- PRIVATE -------------*/
    private static String getCallerClassName(){
        //get caller class name
        String callerClassName = Thread.currentThread().getStackTrace()[3].getClassName();
        String[] arr = callerClassName.split("\\.");

        return arr[arr.length-1];
    }

    /*------------- PROTECTED -------------*/

    /*------------- PUBLIC -------------*/
    public static String getErrorLog(){
        return errorTable.get().toString();
    }

    public static void addHeader(String testName){
        String name = LogType.TEST_STARTED.name;
        String headerLine = String.format("%d-[%s][%s] %s", testCounter++, testName, BROWSER, name + " at " + TestUtils.getCurrentTime());
        logTable.get().add(headerLine);
    }

    public static String getLog(){
        String log = "";
        for(String l : logTable.get()){ log += l + "\n"; }

        return log;
    }

    public static void clear(){
        logTable.get().clear();
        errorTable.get().clear();
    }

    public static void log(LogType type, String text){
        String className = getCallerClassName();

        String name = type.name;
        String newLine = String.format("\t-[%s][%s] : %s", name, className, text);
        logTable.get().add(newLine);
    }

    public static void logError(String text, String message){
        String className = getCallerClassName();

        String name = LogType.ERROR.name;
        String newLine = String.format("\t-[%s][%s] : %s", name, className, text);
        logTable.get().add(newLine);

        errorTable.get().add(message+ "\n\t");
    }

    public static void printLog(){
        String log = getLog();
        String error = getErrorLog();
        if (error!=null) {
            System.out.println(log + "\n" + error);
        }
        else{
            System.out.println(log);
        }
    }

}
