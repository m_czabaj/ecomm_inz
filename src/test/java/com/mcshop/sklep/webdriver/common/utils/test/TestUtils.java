package com.mcshop.sklep.webdriver.common.utils.test;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TestUtils {

    public static String getCurrentTime(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date());
    }
}
