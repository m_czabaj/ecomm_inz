package com.mcshop.sklep.webdriver.common.clipboard;

import java.util.HashMap;

public class Clipboard {
    private static Clipboard instance = null;
    private static HashMap<String, Object> clipboard = new HashMap<>();

    private Clipboard() {}

    public static Clipboard getInstance() {
        if(instance == null)
            instance = new Clipboard();

        return instance;
    }

    public void put(String key, Object val) {
        clipboard.put(key, val);
    }

    public Object get(String key) {
        return clipboard.get(key);
    }
}
