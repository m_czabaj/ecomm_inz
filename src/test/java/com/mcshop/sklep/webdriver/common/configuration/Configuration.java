package com.mcshop.sklep.webdriver.common.configuration;

public class Configuration {

    //environment and runtime configuration
    public static ThreadLocal<String> BROWSER= ThreadLocal.withInitial(() -> System.getProperty("browser", "chrome"));
    public static final String DISABLE_FLASH = System.getProperty("disableFlash", "false");
    public static final String ENVIRONMENT  = System.getProperty("environment", "preprod");
    public static final String REMOTE       = System.getProperty("remote", "false");
    public static final String OS_NAME      = System.getProperty("os.name").toUpperCase();
    public static final String USE_PROXY    = System.getProperty("useProxy", "false");
    public static final String USE_ZAP      = System.getProperty("useZAP", "false");
    public static final String ZAP_ADDRESS  = System.getProperty("zapAddress", "192.168.99.100");
    public static final String ZAP_PORT     = System.getProperty("zapPort", "8090");
    public static final String HUB_HOST     = System.getProperty("hubHost","192.168.99.100");
    public static final String HUB_PORT     = System.getProperty("hubPort", "4444");
    public static final String PROJECT_DIR  = System.getProperty("user.dir");
    public static final String HOST         = System.getProperty("host", "http://192.168.99.100:8080/en/");
    public static final int DRIVER_WAIT     = Integer.parseInt(System.getProperty("driverWait", "4"));
    public static final int SCREEN_HEIGHT   = Integer.parseInt(System.getProperty("screenHeight", "768"));
    public static final int SCREEN_WIDTH    = Integer.parseInt(System.getProperty("screenWidth", "1366"));

    //group names
    public static final String PREPARING_ACCOUNT = "preparingAccount";
    public static final String GROUP_ORDERS = "orders";

}
