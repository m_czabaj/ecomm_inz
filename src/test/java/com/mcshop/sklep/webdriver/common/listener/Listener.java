package com.mcshop.sklep.webdriver.common.listener;

import com.mcshop.sklep.webdriver.common.clipboard.Clipboard;
import com.mcshop.sklep.webdriver.common.logging.PageObjectLogger;
import com.mcshop.sklep.webdriver.common.utils.test.TestUtils;
import com.mcshop.sklep.webdriver.core.browserdriver.BrowserDriver;
import com.mcshop.sklep.webdriver.core.driverprovider.DriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;
import org.testng.*;

public class Listener extends AbstractWebDriverEventListener implements ITestListener, ISuiteListener {

    /*-------------------------------------*
     * FIELDS DECLARATION AND DEFINITION
     *-------------------------------------*/
    private Clipboard clipboard = Clipboard.getInstance();

    /*-------------------------------------*
     * METHODS DEFINITION
     *-------------------------------------*/
    /*------------- PRIVATE -------------*/

    /*------------- PROTECTED -------------*/

    /*------------- PUBLIC -------------*/

    @Override
    public void onTestStart(ITestResult iTestResult) {
        String testName = iTestResult.getName();
        PageObjectLogger.addHeader(testName);
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        String methodName = iTestResult.getName();

        double timeSpent = (double)(iTestResult.getEndMillis() - iTestResult.getStartMillis()) / 1000;
        PageObjectLogger.log(PageObjectLogger.LogType.TEST_SUCCESS, "Test method: "+methodName +" was ended successfully!! Test lasted[s]: " + timeSpent);

        //print log
        PageObjectLogger.printLog();

        //clear log
        PageObjectLogger.clear();
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        String methodName = iTestResult.getName();

        double timeSpent = (double)(iTestResult.getEndMillis() - iTestResult.getStartMillis()) / 1000;
        String screenName = TestUtils.getCurrentTime().replace(":", ".") + "_" + methodName;
        PageObjectLogger.log(PageObjectLogger.LogType.TEST_FAILED, "Test method: "+methodName +" was failed!! Test lasted[s]: " + timeSpent);

        //save screenshot
        BrowserDriver driver = DriverProvider.getActiveDriver();
        driver.saveScreenshot(screenName);

        //print log
        PageObjectLogger.printLog();

        //clear log
        PageObjectLogger.clear();
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        String methodName = iTestResult.getName();

        PageObjectLogger.log(PageObjectLogger.LogType.TEST_SKIPPED, "Test method: "+ methodName+ " was skipped!");

        //print log
        PageObjectLogger.printLog();

        //clear log
        PageObjectLogger.clear();
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        double timeSpent = (double)(iTestResult.getEndMillis() - iTestResult.getStartMillis()) / 1000;
        PageObjectLogger.log(PageObjectLogger.LogType.TEST_FAILED, "Test lasted[s]: " + timeSpent);

        PageObjectLogger.printLog();
        PageObjectLogger.clear();
    }

    @Override
    public void onStart(ITestContext iTestContext) {
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        Long start = iTestContext.getStartDate().getTime();
        Long end = iTestContext.getEndDate().getTime();
        Long duration = (end - start)/1000;
        System.out.println(String.format("Duration %s s", duration));
        PageObjectLogger.printLog();
        PageObjectLogger.clear();
    }

    /*-------------------------------------*
     * Implementation of the AbstractWebDriverEventListener interface methods
     *-------------------------------------*/

    @Override
    public void afterNavigateTo(String url, WebDriver driver) {
    }

    @Override
    public void afterFindBy(By by, WebElement element, WebDriver driver) {
        //addLine("INFO",": found element " + by);
    }

    @Override
    public void afterClickOn(WebElement element, WebDriver driver) {
        //addLine("INFO",": clicked on " + element);
    }

    @Override
    public void beforeFindBy(By by, WebElement element, WebDriver driver) {
        //addLine("INFO",": trying to find element " + by);
    }

    @Override
    public void afterChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
        /*String str = "";
        for(int i=0; i<keysToSend.length; i++) str += keysToSend[i];
        addLine("INFO",": field filled with value: " + str);*/
    }

    @Override
    public void onException(Throwable throwable, WebDriver driver) {
        /*addLine("ERROR", ": step could not be done. See the stack trace");
        addError(throwable.getMessage());*/
    }

    /*-------------------------------------*
     * Implementation of the ISuiteListener interface methods
     *-------------------------------------*/

    @Override
    public void onStart(ISuite iSuite) {

    }

    @Override
    public void onFinish(ISuite iSuite) {
        PageObjectLogger.printLog();
        PageObjectLogger.clear();
    }
}
