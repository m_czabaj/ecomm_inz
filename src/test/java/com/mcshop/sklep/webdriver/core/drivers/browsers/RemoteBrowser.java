package com.mcshop.sklep.webdriver.core.drivers.browsers;

import com.mcshop.sklep.webdriver.common.configuration.Configuration;
import com.mcshop.sklep.webdriver.core.browserdriver.BrowserDriver;
import com.mcshop.sklep.webdriver.core.drivers.BrowserAbstract;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class RemoteBrowser extends BrowserAbstract {
    /*-------------------------------------*
     * FIELDS DECLARATION AND DEFINITION
     *-------------------------------------*/
    private Map<String, DesiredCapabilities> browserCaps = createMap();

    /*-------------------------------------*
     * METHODS DEFINITION
     *-------------------------------------*/
    /*------------- PRIVATE -------------*/
    private HashMap<String, DesiredCapabilities> createMap(){
        HashMap<String, DesiredCapabilities> tmp = new HashMap<>();
        tmp.put("FIREFOX", DesiredCapabilities.firefox());
        tmp.put("CHROME", DesiredCapabilities.chrome());
        tmp.put("IE", DesiredCapabilities.internetExplorer());

        return tmp;
    }

    /*------------- PROTECTED -------------*/

    /*------------- PUBLIC -------------*/
    @Override
    public void setOptions() {
        String browser = Configuration.BROWSER.get().toUpperCase();
        caps = browserCaps.get(browser);
    }

    @Override
    public BrowserDriver create() {
        String hubAddress = String.format("http://%s:%s/wd/hub", Configuration.HUB_HOST, Configuration.HUB_PORT);
        URL hubUrl = null;
        try {
            hubUrl = new URL(hubAddress);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        RemoteWebDriver remote = new RemoteWebDriver(hubUrl, caps);

        return new BrowserDriver(remote);
    }

    @Override
    protected void setProxy() {
        System.getProperties().put("http.proxyHost", Configuration.ZAP_ADDRESS);
        System.getProperties().put("http.proxyPort", Configuration.ZAP_PORT);
        System.getProperties().put("https.proxyHost", Configuration.ZAP_ADDRESS);
        System.getProperties().put("https.proxyPort", Configuration.ZAP_PORT);
        Proxy proxyServer = new Proxy();
        String zapProxyAddress = String.format("%s:%s", Configuration.ZAP_ADDRESS, Integer.parseInt(Configuration.ZAP_PORT));
        proxyServer.setHttpProxy(zapProxyAddress);
        proxyServer.setSslProxy(zapProxyAddress);
        caps.setCapability(CapabilityType.PROXY, proxyServer);
    }
}
