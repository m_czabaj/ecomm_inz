package com.mcshop.sklep.webdriver.core.drivers;

import com.mcshop.sklep.webdriver.common.configuration.Configuration;
import com.mcshop.sklep.webdriver.core.browserdriver.BrowserDriver;
import com.mcshop.sklep.webdriver.core.drivers.browsers.ChromeBrowser;
import com.mcshop.sklep.webdriver.core.drivers.browsers.FirefoxBrowser;
import com.mcshop.sklep.webdriver.core.drivers.browsers.RemoteBrowser;

import java.util.HashMap;
import java.util.Map;

public abstract class BrowserManager {
    /*-------------------------------------*
     * FIELDS DECLARATION AND DEFINITION
     *-------------------------------------*/
    private static final Map<String, Class<? extends BrowserAbstract>> browsers = createMap();

    /*-------------------------------------*
     * METHODS DEFINITION
     *-------------------------------------*/
    /*------------- PRIVATE -------------*/
    private static HashMap<String, Class<? extends BrowserAbstract>> createMap(){
        HashMap<String, Class<? extends BrowserAbstract>> tmp = new HashMap<>();

        tmp.put("CHROME", ChromeBrowser.class);
        tmp.put("FIREFOX", FirefoxBrowser.class);
        tmp.put("REMOTE", RemoteBrowser.class);

        return tmp;
    }

    /*------------- PROTECTED -------------*/

    /*------------- PUBLIC -------------*/
    public static BrowserDriver getInstance(){
        String browser;

        if("true".equalsIgnoreCase(Configuration.REMOTE))
            browser = "REMOTE";
        else
            browser = Configuration.BROWSER.get();

        try {
            return browsers.get(browser.toUpperCase()).newInstance().getInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }
}
