package com.mcshop.sklep.webdriver.core.browserdriver;

import com.mcshop.sklep.webdriver.common.configuration.Configuration;
import com.mcshop.sklep.webdriver.common.logging.PageObjectLogger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class BrowserDriver extends EventFiringWebDriver {
    private final WebDriver driver;

    public BrowserDriver(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public void saveScreenshot(String fileName){
        //get path to current project
        File mediaDir = new File(Configuration.PROJECT_DIR + "/media");

        //get screenshot
        File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        //make destination file handle
        File destFile = new File(mediaDir + "/" + fileName + ".png");

        //check if directory exists
        if( !mediaDir.exists() ) mediaDir.mkdir();

        //save file
        try {
            Files.move(srcFile.toPath(), destFile.toPath());
            PageObjectLogger.log(PageObjectLogger.LogType.INFO, "saved screenshot " + fileName + " in media folder");
        } catch (IOException e) {
            PageObjectLogger.logError("error occured while saving screenshot", e.getMessage());
            e.printStackTrace();
        }
    }
}
