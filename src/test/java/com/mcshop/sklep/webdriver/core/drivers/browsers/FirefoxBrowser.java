package com.mcshop.sklep.webdriver.core.drivers.browsers;

import com.mcshop.sklep.webdriver.common.configuration.Configuration;
import com.mcshop.sklep.webdriver.core.browserdriver.BrowserDriver;
import com.mcshop.sklep.webdriver.core.drivers.BrowserAbstract;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

public class FirefoxBrowser extends BrowserAbstract {
    /*-------------------------------------*
     * FIELDS DECLARATION AND DEFINITION
     *-------------------------------------*/
    private FirefoxProfile profile = new FirefoxProfile();;

    /*-------------------------------------*
     * METHODS DEFINITION
     *-------------------------------------*/
    /*------------- PRIVATE -------------*/

    /*------------- PROTECTED -------------*/

    /*------------- PUBLIC -------------*/
    @Override
    public void setOptions() {
    }

    @Override
    public BrowserDriver create() {
        return new BrowserDriver(new FirefoxDriver(profile));
    }

    @Override
    protected void setProxy() {
        profile.setPreference("network.proxy.type", 1);
        profile.setPreference("network.proxy.http", Configuration.ZAP_ADDRESS);
        profile.setPreference("network.proxy.http_port", Configuration.ZAP_PORT);
    }
}
