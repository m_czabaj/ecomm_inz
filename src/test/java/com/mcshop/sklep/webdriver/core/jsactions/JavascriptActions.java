package com.mcshop.sklep.webdriver.core.jsactions;

import com.mcshop.sklep.webdriver.common.logging.PageObjectLogger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class JavascriptActions {
    /*-------------------------------------*
     * FIELDS DECLARATION AND DEFINITION
     *-------------------------------------*/
    private final JavascriptExecutor js;
    private final WebDriver driver;

    /*-------------------------------------*
     * METHODS DEFINITION
     *-------------------------------------*/
    /*------------- CONSTRUCTORS -------------*/
    public JavascriptActions(WebDriver driver) {
        this.js = (JavascriptExecutor) driver;
        this.driver = driver;
    }

    /*------------- PRIVATE -------------*/

    /*------------- PROTECTED -------------*/

    /*------------- PUBLIC -------------*/
    public void click(String cssSelector) {
        js.executeScript("$('" + cssSelector + "').click()");
    }

    public void click(WebElement element) {
        js.executeScript("$(arguments[0])[0].click()", element);
    }

    public void focus(String cssSelector) {
        js.executeScript("$('" + cssSelector + "').focus()");
    }

    public void focus(WebElement element) {
        js.executeScript("$(arguments[0]).focus()", element);
    }

    public void scrollTo(WebElement element) {
        js.executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public Object execute(String script, WebElement element) {
        return js.executeScript(script, element);
    }

    public Object execute(String script) {
        try {
            Object value = js.executeScript(/*"return " +*/ script);
            return value;
        } catch (UnsupportedOperationException e) {
            PageObjectLogger.logError("UnsupportedOperationException occurred while executing script: " + script, e.getMessage());
            throw e;
        }
    }

    public Object executeAsyncScript(String script) {
        try {
            Object value = js.executeAsyncScript(/*"return " +*/ script);
            return value;
        } catch (UnsupportedOperationException e) {
            PageObjectLogger.logError("UnsupportedOperationException occurred while executing script: " + script, e.getMessage());
            throw e;
        }
    }

}
