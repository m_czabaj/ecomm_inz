package com.mcshop.sklep.webdriver.core.drivers.browsers;

import com.mcshop.sklep.webdriver.common.configuration.Configuration;
import com.mcshop.sklep.webdriver.core.browserdriver.BrowserDriver;
import com.mcshop.sklep.webdriver.core.drivers.BrowserAbstract;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;

public class ChromeBrowser extends BrowserAbstract {
    /*-------------------------------------*
     * FIELDS DECLARATION AND DEFINITION
     *-------------------------------------*/
    private ChromeOptions chromeOptions = new ChromeOptions();

    /*-------------------------------------*
     * METHODS DEFINITION
     *-------------------------------------*/
    /*------------- PRIVATE -------------*/

    /*------------- PROTECTED -------------*/

    /*------------- PUBLIC -------------*/
    @Override
    public void setOptions() {
        chromeOptions.addArguments("start-maximized");
        chromeOptions.addArguments("disable-notifications");
        chromeOptions.addArguments("start-maximized");
        chromeOptions.addArguments("start-fullscreen");

        if ("true".equals(Configuration.USE_ZAP)) {
            chromeOptions.addArguments("ignore-certificate-errors");
        }

        if ("true".equals(Configuration.DISABLE_FLASH)) {
            chromeOptions.addArguments("disable-bundled-ppapi-flash");
        }
    }

    @Override
    public BrowserDriver create() {
        caps.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

        return new BrowserDriver(new ChromeDriver(caps));
    }

    @Override
    protected void setProxy() {
        Proxy proxyServer = new Proxy();
        String zapProxyAddress = String.format("%s:%s", Configuration.ZAP_ADDRESS, Integer.parseInt(Configuration.ZAP_PORT));
        proxyServer.setHttpProxy(zapProxyAddress);
        proxyServer.setSslProxy(zapProxyAddress);
        caps.setCapability(CapabilityType.PROXY, proxyServer);
    }
}
