package com.mcshop.sklep.webdriver.core.wait;

import com.mcshop.sklep.webdriver.common.logging.PageObjectLogger;
import com.mcshop.sklep.webdriver.core.jsactions.JavascriptActions;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class Wait {
    /*-------------------------------------*
     * FIELDS DECLARATION AND DEFINITION
     *-------------------------------------*/
    private static final int DEFAULT_TIMEOUT = 4;
    private static final int DEFAULT_POLLING = 1500;
    private WebDriverWait wait;
    private WebDriver driver;
    private JavascriptActions jsActions;

    /*-------------------------------------*
     * METHODS DEFINITION
     *-------------------------------------*/
    /*------------- CONSTRUCTORS -------------*/
    public Wait(WebDriver webDriver) {
        this.driver = webDriver;
        this.wait = new WebDriverWait(webDriver, DEFAULT_TIMEOUT, DEFAULT_POLLING);
        this.jsActions = new JavascriptActions(driver);
    }

    /*------------- PRIVATE -------------*/
    private void restoreDefaultImplicitWait() {
        changeImplicitWait(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
    }

    private void changeImplicitWait(int value, TimeUnit timeUnit) {
        driver.manage().timeouts().implicitlyWait(value, timeUnit);
    }

    /*------------- PROTECTED -------------*/

    /*------------- PUBLIC -------------*/

    /**
     * Checks if the element is clickable on the browser
     */
    public WebElement forElementClickable(WebElement element) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (TimeoutException e) {
            PageObjectLogger.logError("time out while waiting for element to be clickable", e.getMessage());
            throw e;
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    /**
     * Checks if the element is clickable on the browser
     */
    public WebElement forElementClickable(By by, int timeout) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return new WebDriverWait(driver, timeout).until(ExpectedConditions.elementToBeClickable(by));
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("time out while waiting for element %s to be clickable", by.toString()), e.getMessage());
            throw e;
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public void forNumberElementsToBe(By locator, int expectedNumber){
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            wait.until(ExpectedConditions.numberOfElementsToBe(locator, expectedNumber));
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("time out while waiting for element %s to be clickable", locator.toString()), e.getMessage());
            throw e;
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    /**
     * Checks if the element is clickable on the browser
     */
    public WebElement forElementClickable(By by) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return wait.until(ExpectedConditions.elementToBeClickable(by));
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("time out while waiting for element %s to be clickable", by.toString()), e.getMessage());
            throw e;
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public boolean forElementEnable(By by) {
        try {
            return wait.until(p -> this.driver.findElement(by).isEnabled());
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("time out while waiting for element %s to be enable", by.toString()), e.getMessage());
            throw e;
        } finally {
            restoreDefaultImplicitWait();
        }
    }


    public WebElement forElementVisible(WebElement element, int timeoutSec, int polling) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return new WebDriverWait(driver, timeoutSec, polling).until(ExpectedConditions
                    .visibilityOf(element));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    public WebElement forElementVisible(WebElement element, int timeoutSec) {
        return forElementVisible(element, timeoutSec, 500);
    }

    /**
     * Checks if the element is visible on the browser
     */
    public WebElement forElementVisible(By by) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    /**
     * Wait for element to be either invisible or not present on the DOM.
     */
    public boolean forElementNotVisible(By by) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    /**
     * Wait for element to be either invisible or not present on the DOM.
     */
    public boolean forElementNotVisible(By by, int timeout, int polling) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return new WebDriverWait(driver, timeout, polling).until(
                    ExpectedConditions.invisibilityOfElementLocated(by));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    /**
     * Wait for element to be either invisible or not present on the DOM.
     */
    public boolean forElementNotVisible(By by, Duration timeout) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return new WebDriverWait(driver, timeout.getSeconds()).until(
                    ExpectedConditions.invisibilityOfElementLocated(by));
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    /**
     * Wait for element text to be as expected
     */
    public boolean forTextToBe(By by, String text) {
        try {
            return wait.until(ExpectedConditions.textToBePresentInElementLocated(by, text));
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("time out while waiting for element %s text to be %s", by.toString(), text), e.getMessage());
            throw e;
        }
    }

    /**
     * Wait for element text to be as expected
     */
    public boolean forTextMatches(By by, Pattern pattern) {
        try {
            return wait.until(ExpectedConditions.textMatches(by, pattern));
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("time out while waiting for element %s text matches to %s", by.toString(), pattern.toString()), e.getMessage());
            throw e;
        }
    }

    public boolean forContainingText(By by, String text) {
        try {
            return wait.until(ExpectedConditions.textToBePresentInElementLocated(by, text));
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("time out while waiting for element %s text matches to %s", by.toString(), text.toString()), e.getMessage());
            throw e;
        }
    }

    /**
     * Wait for attribute value contains specified text
     */
    public boolean forAttributeContains(By by, String attributeName, String text) {
        try {
            return wait.until(ExpectedConditions.attributeContains(by, attributeName, text));
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("time out while waiting for element %s %s contains %s", by.toString(), attributeName, text), e.getMessage());
            throw e;
        }
    }

    /**
     * Wait for attribute value contains specified text
     */
    public boolean forAttributeContains(By by, String attributeName, String text, int timeOut) {
        changeImplicitWait(250, TimeUnit.MILLISECONDS);
        try {
            return new WebDriverWait(driver, timeOut).until(ExpectedConditions.attributeContains(by, attributeName, text));
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("time out while waiting for element %s %s contains %s", by.toString(), attributeName, text), e.getMessage());
            throw e;
        } finally {
            restoreDefaultImplicitWait();
        }
    }

    /**
     * Wait for attribute value contains specified text
     */
    public boolean forAttributeContains(WebElement el, String attributeName, String text) {
        try {
            return wait.until(ExpectedConditions.attributeContains(el, attributeName, text));
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("time out while waiting for element %s %s contains %s", el.toString(), attributeName, text), e.getMessage());
            throw e;
        }
    }

    /**
     * Wait for attribute value contains specified text
     */
    public boolean forAttributeContainsXpath(String xpath, String attributeName, String text) {
        By by = By.xpath(xpath);
        return forAttributeContains(by, attributeName, text);
    }

    /**
     * Wait for attribute value to be as expected
     */
    public boolean forAttributeToBe(By by, String attributeName, String expectedValue) {
        try {
            return wait.until(ExpectedConditions.attributeToBe(by, attributeName, expectedValue));
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("time out while waiting for element %s attribute to be %s", by.toString(), expectedValue), e.getMessage());
            throw e;
        }
    }

    public boolean forElementEnabled(By by) {
        if (this.driver.findElement(by).isEnabled())
            return true;
        return false;

    }

    /**
     * Wait for attribute value to be as expected
     */
    public boolean forAttributeToBe(By by, String attributeName, String expectedValue, int waitPollingMilis) {
        try {
            return new WebDriverWait(driver, 5, waitPollingMilis).until(ExpectedConditions.attributeToBe(by, attributeName, expectedValue));
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("time out while waiting for element %s attribute to be %s", by.toString(), expectedValue), e.getMessage());
            throw e;
        }
    }

    /**
     * Wait for element to be selected
     */
    public boolean forElementToBeSelected(By locator) {
        try {
            return wait.until(ExpectedConditions.elementToBeSelected(locator));
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("time out while waiting for element %s is selected", locator.toString()), e.getMessage());
            throw e;
        }
    }

    /**
     * Checks if the element is clickable on the browser
     */
    public WebElement forElementClickableXpath(String xpath) {
        By locator = By.xpath(xpath);
        return forElementClickable(locator);
    }

    /**
     * Checks if the element is clickable on the browser
     */
    public WebElement forElementClickableCss(String css) {
        By locator = By.cssSelector(css);
        return forElementClickable(locator);
    }

    public boolean forStalnessOf(WebElement el) {
        try {
            return wait.until(ExpectedConditions.stalenessOf(el));
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("time out while waiting for element %s vanishing", el.toString()), e.getMessage());
            throw e;
        }
    }

    public boolean javascriptReturns(String script, String expectedValue) {
        try {
            return wait.until((ExpectedCondition<Boolean>) webDriver -> {
                String value = (String) jsActions.execute(script);
                return value.equals(expectedValue);
            });
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("time out while waiting for js returns value: %s.", expectedValue), e.getMessage());
            throw e;
        }
    }

    public boolean waitForUrlContains(String fraction) {
        try {
            return wait.until(ExpectedConditions.urlContains(fraction));
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("time out while waiting for url contain %s", fraction), e.getMessage());
            throw e;
        }
    }

    public WebElement scrollForElementClickable(By by) throws NoSuchElementException {
        try {
            this.jsActions.scrollTo(this.driver.findElement(by));
            this.forElementEnable(by);
            return this.forElementClickable(by);
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("Time out while waiting for scrollable element %s"), e.getMessage());
            throw e;
        }
    }

    public WebElement waitForSelectedElement(Select select, By by, Integer value) {
        try {
            this.forElementClickable(by).click();
            select = new Select(this.driver.findElement(by));
            select.selectByValue(value.toString());
            return this.forElementClickable(by);
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("Time out while waiting for selected element %s"), e.getMessage());
            throw e;
        }
    }
    public WebElement waitForSelectedElement(Select select, By by, String value) {
        try {
            this.forElementClickable(by).click();
            select = new Select(this.driver.findElement(by));
            select.selectByValue(value);
            return this.forElementClickable(by);
        } catch (TimeoutException e) {
            PageObjectLogger.logError(String.format("Time out while waiting for selected element %s"), e.getMessage());
            throw e;
        }
    }
}
