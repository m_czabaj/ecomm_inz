package com.mcshop.sklep.webdriver.core.driverprovider;

import com.mcshop.sklep.webdriver.core.browserdriver.BrowserDriver;
import com.mcshop.sklep.webdriver.core.drivers.BrowserManager;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public abstract class DriverProvider {
    /*-------------------------------------*
     * FIELDS DECLARATION AND DEFINITION
     *-------------------------------------*/
    private static final ThreadLocal<List<BrowserDriver>> drivers = ThreadLocal.withInitial(ArrayList::new);
    private static ThreadLocal<Integer> ACTIVE_BROWSER_INDEX = ThreadLocal.withInitial(() -> 0);
    private DriverProvider() {}

    /*-------------------------------------*
     * METHODS DEFINITION
     *-------------------------------------*/
    /*------------- PRIVATE -------------*/
    private static void newInstance() {
        drivers.get().add(BrowserManager.getInstance());
    }

    private static BrowserDriver getBrowserDriver(int index) {
        for (; drivers.get().size() <= index;) {
            newInstance();
        }

        return drivers.get().get(index);
    }

    /*------------- PROTECTED -------------*/

    /*------------- PUBLIC -------------*/
    public static BrowserDriver getActiveDriver() {
        return getBrowserDriver(ACTIVE_BROWSER_INDEX.get());
    }

    public static BrowserDriver switchActiveWindow(int index) {
        ACTIVE_BROWSER_INDEX.set(index);
        return getActiveDriver();
    }

    public static void switchToWindow(String windowName){
        try {
            getActiveDriver()
                    .switchTo()
                    .window(windowName);
        }
        catch(NoSuchElementException ne){
            ne.printStackTrace();
        }
    }

    public static void close() {
        for (BrowserDriver webDriver : drivers.get()) {
            if (webDriver != null) {
                try {
                    webDriver.quit();
                }catch (UnsatisfiedLinkError | NoClassDefFoundError | NullPointerException e){
                    e.printStackTrace();
                }
            }
        }
        drivers.get().clear();
        ACTIVE_BROWSER_INDEX.set(0);
    }

}
