package com.mcshop.sklep.webdriver.core.drivers;

import com.mcshop.sklep.webdriver.common.configuration.Configuration;
import com.mcshop.sklep.webdriver.common.listener.Listener;
import com.mcshop.sklep.webdriver.core.browserdriver.BrowserDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public abstract class BrowserAbstract {
    /*-------------------------------------*
     * FIELDS DECLARATION AND DEFINITION
     *-------------------------------------*/
    protected DesiredCapabilities caps = new DesiredCapabilities();

    /*-------------------------------------*
     * METHODS DEFINITION
     *-------------------------------------*/
    /*------------- PRIVATE -------------*/

    /*------------- PROTECTED -------------*/
    protected void setBrowserLogging(Level logLevel) {
        LoggingPreferences loggingprefs = new LoggingPreferences();
        loggingprefs.enable(LogType.BROWSER, logLevel);
        caps.setCapability(CapabilityType.LOGGING_PREFS, loggingprefs);
    }

    protected void setTimeouts(BrowserDriver webDriver) {
        webDriver.manage().timeouts().implicitlyWait(Configuration.DRIVER_WAIT, TimeUnit.SECONDS);
    }

    protected void setListeners(BrowserDriver webDriver) {
        webDriver.register(new Listener());
    }

    /**
     * Set Proxy instance for a Browser instance
     */
    protected abstract void setProxy();

    /*------------- PUBLIC -------------*/
    /**
     * Get a ready to work instance for chosen browser
     *
     * @return
     */
    public BrowserDriver getInstance() {
        if ("true".equals(Configuration.USE_ZAP)) setProxy();
        setOptions();
        BrowserDriver webdriver = create();
        setTimeouts(webdriver);
        setListeners(webdriver);

        return webdriver;
    }

    /**
     * Set Browser specific options, before creating a working instance
     */
    public abstract void setOptions();

    /**
     * Create a working instance of a Browser
     *
     * @return
     */
    public abstract BrowserDriver create();
}
