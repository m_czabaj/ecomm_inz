package com.mcshop.sklep.webdriver.ui.components;

import com.mcshop.sklep.webdriver.ui.pageobjects.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.mcshop.sklep.webdriver.common.logging.PageObjectLogger.*;

public class Header extends BaseComponenet{

    /* Top of the header: header-nav*/
    private By aContactUs = By.cssSelector("div#contact-link a");
    private By aLogIn = By.cssSelector("a[title='Log in to your customer account']");
    private By aLogOut = By.cssSelector("a.logout");
    private By aAccountData = By.cssSelector("a.account");
    private By spanProductsCout = By.cssSelector("span.cart-products-count");
    private By iCart = By.cssSelector("i.shopping-cart");
    /* Bottom of the header: header-top */
    private By divLogo = By.cssSelector("div#_desktop_logo");
    private String menuItemXpath = ".//a[contains(normalize-space(),'%s')]";
    private String submenuItemXpath = menuItemXpath + "../" + menuItemXpath;
    private By inputSearch = By.cssSelector("div#search_widget input[type=text]");
    private By buttonSearchSubmit = By.cssSelector("button[type=submit]");

    public Header(WebElement locator) {
        super(locator);
    }

    public ContactPageObject navigateToContactForm() {
        componentLocator.findElement(aContactUs).click();
        log(INFO, "Navigated to contact page");
        return new ContactPageObject();
    }

    public LoginPageObject navigateToLoginPage() {
        componentLocator.findElement(aLogIn).click();
        log(INFO, "Navigated to login page");
        return new LoginPageObject();
    }

    public LoginPageObject logOut() {
        componentLocator.findElement(aLogOut).click();
        log(INFO, "User has been logged out");
        return new LoginPageObject();
    }

    public AccountPageObject navigateToAccount(){
        componentLocator.findElement(aAccountData).click();
        log(INFO, "Navigated to user account");
        return new AccountPageObject();
    }

    public int getCartProductsCount() {
        String number = componentLocator.findElement(spanProductsCout).getText();
        return Integer.parseInt(number);
    }

    public CartPageObject cart() {
        componentLocator.findElement(iCart).click();
        log(INFO, "Navigated to cart");
        return new CartPageObject();
    }

    public HomePageObject clickLogo() {
        componentLocator.findElement(divLogo).click();
        log(INFO, "Clicked on logo");
        return new HomePageObject();
    }

    public CategoryPageObject openCategory(String categoryName) {
        String categoryXpath = String.format(menuItemXpath, categoryName);
        componentLocator.findElement(By.xpath(categoryXpath)).click();
        log(INFO, "Opened category: " + categoryName);
        return new CategoryPageObject();
    }

    public SearchPageObject searchProduct(String searchText) {
        componentLocator.findElement(inputSearch).sendKeys(searchText);
        componentLocator.findElement(buttonSearchSubmit).click();
        log(INFO, "Searched for product: " + searchText);
        return new SearchPageObject();
    }

    public CategoryPageObject openSubcategory(String categoryName, String subcategoryName) {
        String subcategoryXpath = String.format(submenuItemXpath, categoryName, subcategoryName);
        componentLocator.findElement(By.xpath(subcategoryXpath)).click();
        log(INFO, "Opened subcategory: " + categoryName);
        return new CategoryPageObject();
    }

    public String getLoggedUserData(){
        return componentLocator.findElement(aAccountData).getAttribute("innerText");
    }


}
