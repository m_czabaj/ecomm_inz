package com.mcshop.sklep.webdriver.ui.pageobjects;

import com.mcshop.sklep.webdriver.ui.BasePageObject;
import org.openqa.selenium.By;

public class OrderConfirmationPageObject extends BasePageObject {

    private By pageTitle = By.cssSelector("section#content-hook_order_confirmation h3");

    public String getConfirmationText(){
        // substring to get rid of the thick
        return driver.findElement(pageTitle).getText().trim().substring(1);
    }
}
