package com.mcshop.sklep.webdriver.ui.components;

import com.mcshop.sklep.webdriver.ui.pageobjects.CategoryPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.mcshop.sklep.webdriver.common.logging.PageObjectLogger.*;

public class FiltersSection extends BaseComponenet {

    private By buttonSearchAllFilters = By.cssSelector("button.js-search-filters-clear-all");
    private String aFacetXpath = ".//a[contains(@class, 'js-search-link') and contains(text(), '%s')]";

    public FiltersSection(WebElement elementLocator) {
        super(elementLocator);
    }

    public CategoryPageObject selectFacet(String facetName){
        String facetXpath = String.format(aFacetXpath, facetName);
        componentLocator.findElement(By.xpath(facetXpath)).click();
        log(INFO, String.format("Selected '%s' facet", facetName));
        return new CategoryPageObject();
    }

    public CategoryPageObject clearAllFilters(){
        componentLocator.findElement(buttonSearchAllFilters).click();
        log(INFO, "Cleared all filters");
        return new CategoryPageObject();
    }
}
