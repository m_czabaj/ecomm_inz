package com.mcshop.sklep.webdriver.ui.pageobjects;

import com.mcshop.sklep.webdriver.ui.BasePageObject;
import com.mcshop.sklep.webdriver.ui.components.AddressForm;
import org.openqa.selenium.By;


public class AddressesPageObject extends BasePageObject {

    private By sectionContent = By.cssSelector("section#content");

    public AddressForm addressForm(){
        return new AddressForm(driver.findElement(sectionContent));
    }
}
