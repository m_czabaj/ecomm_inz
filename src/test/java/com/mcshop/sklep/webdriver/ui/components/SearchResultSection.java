package com.mcshop.sklep.webdriver.ui.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

import static com.mcshop.sklep.webdriver.common.logging.PageObjectLogger.*;

public class SearchResultSection extends BaseComponenet {

    private String aSortOptionXpath = ".//a[contains(@class, 'js-search-link') and contains(text(), '%s')]";
    private By buttonSortOptionDropdow = By.cssSelector("div.products-sort-order button");
    private By articleProduct = By.cssSelector("article.product-miniature");

    public SearchResultSection(WebElement elementLocator) {
        super(elementLocator);
    }

    public List<ProductTile> getAllProducts(){
        return componentLocator.findElements(articleProduct).stream().map(ProductTile::new).collect(Collectors.toList());
    }

    public SearchResultSection changeSortingTo(String optionName){
        componentLocator.findElement(buttonSortOptionDropdow).click();
        By optionLocator = By.xpath(String.format(aSortOptionXpath, optionName));
        componentLocator.findElement(optionLocator).click();
        wait.forContainingText(buttonSortOptionDropdow, optionName);
        log(INFO, "Changed sorting to: " + optionName);
        return this;
    }


}
