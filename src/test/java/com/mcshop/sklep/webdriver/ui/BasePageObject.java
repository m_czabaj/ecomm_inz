package com.mcshop.sklep.webdriver.ui;


import com.mcshop.sklep.webdriver.common.logging.PageObjectLogger;
import com.mcshop.sklep.webdriver.core.browserdriver.BrowserDriver;
import com.mcshop.sklep.webdriver.core.jsactions.JavascriptActions;
import com.mcshop.sklep.webdriver.core.wait.Wait;
import com.mcshop.sklep.webdriver.ui.components.Header;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static com.mcshop.sklep.webdriver.core.driverprovider.DriverProvider.*;

public abstract class BasePageObject{

    private By headerLocator = By.cssSelector("header#header");
    protected PageObjectLogger.LogType INFO = PageObjectLogger.LogType.INFO;
    protected PageObjectLogger.LogType WARNING = PageObjectLogger.LogType.WARNING;
    protected BrowserDriver driver;
    protected Wait waitFor;
    protected JavascriptActions jsActions;
    protected Actions builder;

    /*
     * Constructors
     */

    public BasePageObject(){
        driver = getActiveDriver();
        waitFor = new Wait(driver);
        jsActions = new JavascriptActions(driver);
        builder = new Actions(driver);
    }

    /*
     * Private methods
     */

    /*
     * Public methods
     */

    public Header header(){
        return new Header(driver.findElement(headerLocator));
    }

    protected BigDecimal getBigDecimalFromString(String str) {
        String stringNumber = str.replaceAll("(?!\\d|\\.).", ""); //replace everything with an empty string except numbers and dot
        return new BigDecimal(stringNumber).setScale(2, RoundingMode.UNNECESSARY);
    }
}
