package com.mcshop.sklep.webdriver.ui.pageobjects;

import com.mcshop.sklep.webdriver.ui.BasePageObject;
import com.mcshop.sklep.webdriver.ui.components.SearchResultSection;
import org.openqa.selenium.By;

public class SearchPageObject extends BasePageObject {

    private By h2ListHeader = By.cssSelector("h2#js-product-list-header");
    private By sectionProducts = By.cssSelector("section#products");

    public String getTitle(){
        return driver.findElement(h2ListHeader).getText();
    }

    public SearchResultSection productSection(){
        return new SearchResultSection(driver.findElement(sectionProducts));
    }



}
