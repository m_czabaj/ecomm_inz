package com.mcshop.sklep.webdriver.ui.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.mcshop.sklep.webdriver.common.logging.PageObjectLogger.*;

public class SavedAddressTile extends BaseComponenet {

    private By aDeleteAddress = By.cssSelector("a[data-link-action='delete-address']");
    private By aEditAddress = By.cssSelector("a[data-link-action='edit-address']");
    private By divAddressForm = By.cssSelector("div.js-address-form");

    public SavedAddressTile(WebElement elementLocator) {
        super(elementLocator);
    }

    public AddressForm editAddressForm(){
        componentLocator.findElement(aEditAddress).click();
        log(INFO, "Gone to edit address form");
        return new AddressForm(wait.forElementVisible(divAddressForm));
    }

    public void deleteAddress(){
        componentLocator.findElement(aDeleteAddress).click();
        log(INFO, "Address deleted");
    }
}
