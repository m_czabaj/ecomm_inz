package com.mcshop.sklep.webdriver.ui.components;

import com.mcshop.sklep.webdriver.ui.pageobjects.CartPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.mcshop.sklep.webdriver.common.logging.PageObjectLogger.*;

public class BlockCartModal extends BaseComponenet{

    private By buttonFinishOrder = By.cssSelector("a.btn-primary");

    public BlockCartModal(WebElement locator){
        super(locator);
    }

    public CartPageObject goToCart(){
        componentLocator.findElement(buttonFinishOrder).click();
        log(INFO, "Proceeded to checkout");
        return new CartPageObject();
    }
}
