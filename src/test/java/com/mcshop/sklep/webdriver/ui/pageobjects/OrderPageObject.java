package com.mcshop.sklep.webdriver.ui.pageobjects;

import com.mcshop.sklep.webdriver.ui.BasePageObject;
import com.mcshop.sklep.webdriver.ui.components.AddressForm;
import com.mcshop.sklep.webdriver.ui.components.DeliveryMethod;
import com.mcshop.sklep.webdriver.ui.components.PaymentMethod;
import org.openqa.selenium.By;

public class OrderPageObject extends BasePageObject {

    private By addressFormLocator = By.cssSelector("section#checkout-addresses-step");
    private By personalInformationLocator = By.cssSelector("section#checkout-personal-information-step");
    private By deliverySectionLocator = By.cssSelector("section#checkout-delivery-step");
    private By paymentSection = By.cssSelector("section#checkout-payment-step");

    public OrderPageObject(){
        super();
    }

    public AddressForm addressForm(){
        return new AddressForm(driver.findElement(addressFormLocator));
    }

    public AddressForm personalInformationForm(){
        return new AddressForm(driver.findElement(personalInformationLocator));
    }

    public DeliveryMethod deliveryMethod() { return  new DeliveryMethod(driver.findElement(deliverySectionLocator)); }

    public PaymentMethod paymentMethod() { return new PaymentMethod(driver.findElement(paymentSection)); }
}
