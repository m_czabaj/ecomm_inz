package com.mcshop.sklep.webdriver.ui.pageobjects;

import com.mcshop.sklep.webdriver.ui.BasePageObject;
import com.mcshop.sklep.webdriver.ui.components.RegisterForm;
import org.openqa.selenium.By;

public class CreateAccountPageObject extends BasePageObject {

    private By registerFormLocator = By.cssSelector("section.register-form");

    public CreateAccountPageObject(){
        super();
    }

    public RegisterForm registerForm(){
        return new RegisterForm(driver.findElement(registerFormLocator));
    }
}
