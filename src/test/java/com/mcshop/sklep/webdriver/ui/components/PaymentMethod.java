package com.mcshop.sklep.webdriver.ui.components;

import com.mcshop.sklep.webdriver.ui.pageobjects.OrderConfirmationPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.mcshop.sklep.webdriver.common.logging.PageObjectLogger.*;

public class PaymentMethod extends BaseComponenet{

    private By radioCheckPayment = By.cssSelector("label[for='payment-option-1']");
    private By radioBankTransferPayment = By.cssSelector("label[for='payment-option-2']");
    private By checkTermsAgreement = By.cssSelector("input[id^=conditions_to_approve]");
    private By buttonPaymentConfirmation = By.cssSelector("div#payment-confirmation button");

    public PaymentMethod(WebElement locator){
        super(locator);
        wait.forElementVisible(locator, 2);
    }

    public PaymentMethod payWithCheck(){
        componentLocator.findElement(radioCheckPayment).click();
        log(INFO, "Chosen payment method: check");
        return this;
    }

    public PaymentMethod makeBankTransfer(){
        componentLocator.findElement(radioBankTransferPayment).click();
        log(INFO, "Chosen payment method: bank transfer");
        return this;
    }

    public PaymentMethod checkAgreement(){
        componentLocator.findElement(checkTermsAgreement).click();
        log(INFO, "Agreed to conditions");
        return this;
    }

    public OrderConfirmationPageObject confirmPayment(){
        componentLocator.findElement(buttonPaymentConfirmation).click();
        log(INFO, "Payment confirmed");
        return new OrderConfirmationPageObject();
    }
}
