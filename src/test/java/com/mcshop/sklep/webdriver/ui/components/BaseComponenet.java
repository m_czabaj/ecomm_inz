package com.mcshop.sklep.webdriver.ui.components;

import com.mcshop.sklep.webdriver.common.logging.PageObjectLogger;
import com.mcshop.sklep.webdriver.core.wait.Wait;
import org.openqa.selenium.WebElement;

import static com.mcshop.sklep.webdriver.core.driverprovider.DriverProvider.*;

public class BaseComponenet {

    protected WebElement componentLocator;
    protected Wait wait;
    protected PageObjectLogger.LogType INFO = PageObjectLogger.LogType.INFO;
    protected PageObjectLogger.LogType WARNING = PageObjectLogger.LogType.WARNING;

    public BaseComponenet(WebElement elementLocator){
        this.componentLocator = elementLocator;
        wait = new Wait(getActiveDriver());
    }
}
