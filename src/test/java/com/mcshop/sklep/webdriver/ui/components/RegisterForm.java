package com.mcshop.sklep.webdriver.ui.components;

import com.mcshop.sklep.helpers.AddressDataHelper;
import com.mcshop.sklep.webdriver.ui.pageobjects.HomePageObject;
import com.mcshop.sklep.webdriver.ui.pageobjects.LoginPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.mcshop.sklep.webdriver.common.logging.PageObjectLogger.*;

public class RegisterForm extends BaseComponenet{

    private By aLogIn = By.xpath(".//a[text()='Zaloguj się!']");
    private String radioOptionXpath = ".//label[normalize-space()='%s']";
    private By inputFirstname = By.cssSelector("input[name=firstname]");
    private By inputLastname = By.cssSelector("input[name=lastname]");
    private By inputEmail = By.cssSelector("input[name=email]");
    private By inputPassword = By.cssSelector("input[name=password]");
    private By inputBirthday = By.cssSelector("input[name=birthday]");
    private By checkboxGDPR = By.cssSelector("input[name=psgdpr]");
    private By buttonSubmit = By.cssSelector("button[type=submit]");

    public RegisterForm(WebElement locator) {
        super(locator);
    }

    public LoginPageObject navigateToLoginPage(){
        componentLocator.findElement(aLogIn).click();
        log(INFO, "Navigated to login page");
        return new LoginPageObject();
    }

    public RegisterForm chooseGender(String genderName) {
        String radioXpath = String.format(radioOptionXpath, genderName);
        componentLocator.findElement(By.xpath(radioXpath)).click();
        log(INFO, "Chosen gender: " + genderName);
        return this;
    }

    public RegisterForm approveGDPR(){
        componentLocator.findElement(checkboxGDPR).click();
        log(INFO, "Approved GPDR consent.");
        return this;
    }

    public RegisterForm fillFirstName(String firstname) {
        componentLocator.findElement(inputFirstname).sendKeys(firstname);
        log(INFO, "Filled firstname input with string: " + firstname);
        return this;
    }

    public RegisterForm fillLastName(String lastname) {
        componentLocator.findElement(inputLastname).sendKeys(lastname);
        log(INFO, "Filled lastname input with string: " + lastname);
        return this;
    }

    public RegisterForm fillEmail(String email) {
        componentLocator.findElement(inputEmail).sendKeys(email);
        log(INFO, "Filled email input with string: " + email);
        return this;
    }

    public RegisterForm fillPassword(String password){
        componentLocator.findElement(inputPassword).sendKeys(password);
        log(INFO, "Filled password input with string: " + password);
        return this;
    }

    public RegisterForm fillBrithDate(String date){
        componentLocator.findElement(inputBirthday).sendKeys(date);
        log(INFO, "Filled date input with string: " + date);
        return this;
    }

    public HomePageObject submitForm(){
        componentLocator.findElement(buttonSubmit).click();
        log(INFO, "Form has been submitted");
        return new HomePageObject();
    }

    public HomePageObject fillAndSendEntireFormWith(AddressDataHelper userData){
        return fillFirstName(userData.firstName)
                .fillLastName(userData.lastName)
                .fillEmail(userData.email)
                .fillPassword(userData.password)
                .fillBrithDate(userData.birthdate)
                .approveGDPR()
                .submitForm();
    }
}
