package com.mcshop.sklep.webdriver.ui.pageobjects;

import com.mcshop.sklep.webdriver.ui.BasePageObject;
import com.mcshop.sklep.webdriver.ui.components.FiltersSection;
import com.mcshop.sklep.webdriver.ui.components.SearchResultSection;
import org.openqa.selenium.By;

public class CategoryPageObject extends BasePageObject {

    private By divFilters = By.cssSelector("div#search_filters");
    private By sectionProducts = By.cssSelector("section#products");
    private By sectionActiveFilters = By.cssSelector("section#js-active-search-filters");

    public FiltersSection filters(){
        return new FiltersSection(driver.findElement(divFilters));
    }

    public SearchResultSection productsSection(){
        return new SearchResultSection(driver.findElement(sectionProducts));
    }

}
