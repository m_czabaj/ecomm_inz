package com.mcshop.sklep.webdriver.ui.pageobjects;

import com.mcshop.sklep.webdriver.ui.BasePageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import static com.mcshop.sklep.webdriver.common.logging.PageObjectLogger.*;

public class ContactPageObject extends BasePageObject {

    private By selectSubject = By.cssSelector("select[name=id_contact]");
    private By inputEmail = By.cssSelector("input[name=from]");
    private By textareaMessage = By.cssSelector("textarea[name=message]");
    private By inputSend = By.cssSelector("input[name=submitMessage]");
    private By liConfirmationMessage = By.cssSelector("div.alert li");

    public ContactPageObject selectSubjectByIndex(int index){
        Select subjectList = new Select(driver.findElement(selectSubject));
        subjectList.selectByIndex(index);
        log(INFO, "Selected subject with index: " + index);
        return this;
    }

    public ContactPageObject selectSubjectByName(String name){
        Select subjectList = new Select(driver.findElement(selectSubject));
        subjectList.selectByVisibleText(name);
        log(INFO, "Selected subject with text: " + name);
        return this;
    }

    public ContactPageObject fillEmail(String email){
        driver.findElement(inputEmail).sendKeys(email);
        log(INFO, "Filled email input with string: " + email);
        return this;
    }

    public ContactPageObject fillMessage(String message){
        driver.findElement(textareaMessage).sendKeys(message);
        log(INFO, "Filled message input with string: " + message);
        return this;
    }

    public ContactPageObject sendForm(){
        driver.findElement(inputSend).click();
        log(INFO, "Contact form has been sent");
        return this;
    }

    public String getMessage(){
        return driver.findElement(liConfirmationMessage).getText();
    }

}
