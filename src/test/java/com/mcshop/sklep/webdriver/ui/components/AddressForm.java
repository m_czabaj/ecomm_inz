package com.mcshop.sklep.webdriver.ui.components;

import com.mcshop.sklep.helpers.AddressDataHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

import static com.mcshop.sklep.webdriver.common.logging.PageObjectLogger.*;
import static com.mcshop.sklep.webdriver.core.driverprovider.DriverProvider.*;
import static java.util.stream.Collectors.*;

public class AddressForm extends BaseComponenet{
    
    private By inputFirstname = By.cssSelector("input[name='firstname']");
    private By inputLastname = By.cssSelector("input[name='lastname']");
    private By inputEmail = By.cssSelector("input[name='email']");
    private By inputCompany = By.cssSelector("input[name='company']");
    private By inputVAT = By.cssSelector("input[name='vat_number']");
    private By inputAddress = By.cssSelector("input[name='address1']");
    private By inputAddressAddition = By.cssSelector("input[name='address2']");
    private By inputCity = By.cssSelector("input[name='city']");
    private By inputPostCode = By.cssSelector("input[name='postcode']");
    private By inputPhone = By.cssSelector("input[name='phone']");
    private By checkboxGDPR = By.cssSelector("input[name=psgdpr]");
    private By selectCountry = By.cssSelector("select[name='id_country']");
    private By buttonSave = By.cssSelector("button.btn-primary");
    private By elementAddNewAddress = By.xpath(".//*[@*='add-address']"); //
    private By articleSavedAddressItem = By.cssSelector("article[class^=address]");

    public AddressForm(WebElement locator){
        super(locator);
    }

    public AddressForm fillPersonalInformation(AddressDataHelper addressData) {
        fillFirstname(addressData.firstName);
        fillLastname(addressData.lastName);
        fillEmail(addressData.email);
        approveGDPR();
        return this;
    }

    public AddressForm fillEntireAddress(AddressDataHelper addressData) {
        fillFirstname(addressData.firstName);
        fillLastname(addressData.lastName);
        fillAddress(addressData.address);
        fillCity(addressData.city);
        fillPostcode(addressData.postalCode);
        return this;
    }

    public AddressForm fillEntireAddressIfNoneExists(AddressDataHelper addressData) {
        if(savedAddressesList().isEmpty()) {
            fillEntireAddress(addressData);
        }
        return this;
    }

    public AddressForm approveGDPR(){
        componentLocator.findElement(checkboxGDPR).click();
        log(INFO, "Approved GDPR consent.");
        return this;
    }

    public AddressForm fillFirstname(String fn){
        componentLocator.findElement(inputFirstname).sendKeys(fn);
        log(INFO, "Filled firstname input with string: " + fn);
        return this;
    }

    public AddressForm fillLastname(String ln){
        componentLocator.findElement(inputLastname).sendKeys(ln);
        log(INFO, "Filled lastname input with string: " + ln);
        return this;
    }

    public AddressForm fillEmail(String email){
        componentLocator.findElement(inputEmail).sendKeys(email);
        log(INFO, "Filled email input with string: " + email);
        return this;
    }

    public AddressForm fillCompany(String company){
        componentLocator.findElement(inputCompany).sendKeys(company);
        log(INFO, "Filled company input with string: " + company);
        return this;
    }

    public AddressForm fillVat(String vat){
        componentLocator.findElement(inputVAT).sendKeys(vat);
        log(INFO, "Filled VAT input with string: " + vat);
        return this;
    }

    public AddressForm fillAddress(String address){
        componentLocator.findElement(inputAddress).sendKeys(address);
        log(INFO, "Filled address input with string: " + address);
        return this;
    }

    public AddressForm fillAddressAddition(String addition) {
        componentLocator.findElement(inputAddressAddition).sendKeys(addition);
        log(INFO, "Filled additional address input with string: " + addition);
        return this;
    }

    public AddressForm fillCity(String city){
        componentLocator.findElement(inputCity).sendKeys(city);
        log(INFO, "Filled city input with string: " + city);
        return this;
    }

    public AddressForm fillPostcode(String postCode){
        componentLocator.findElement(inputPostCode).sendKeys(postCode);
        log(INFO, "Filled post code input with string: " + postCode);
        return this;
    }

    public AddressForm fillPhone(String phone){
        componentLocator.findElement(inputPhone).sendKeys(phone);
        log(INFO, "Filled phone input with string: " + phone);
        return this;
    }

    public AddressForm chooseCountryByIndex(int index){
        WebElement list = componentLocator.findElement(selectCountry);
        Select select = new Select(list);
        select.selectByIndex(index);
        log(INFO, "From select, chosen country with index: " + index);
        return this;
    }

    public AddressForm chooseCountryByValue(String name){
        WebElement list = componentLocator.findElement(selectCountry);
        Select select = new Select(list);
        select.selectByValue(name);
        log(INFO, "From select, chosen country with name: " + name);
        return this;
    }

    public AddressForm addNewAddress(){
        if(!componentLocator.findElements(elementAddNewAddress).isEmpty()){
            componentLocator.findElement(elementAddNewAddress).click();
        }
        log(INFO, "Opened address form to add new address.");
        return new AddressForm(getActiveDriver().findElement(By.cssSelector("div.js-address-form")));
    }

    public List<SavedAddressTile> savedAddressesList(){
        return componentLocator.findElements(articleSavedAddressItem).stream().map(SavedAddressTile::new).collect(toList());
    }

    public void saveForm(){
        componentLocator.findElement(buttonSave).click();
        log(INFO, "Address form has been saved");
    }
}
