package com.mcshop.sklep.webdriver.ui.pageobjects;

import com.mcshop.sklep.webdriver.ui.BasePageObject;
import org.openqa.selenium.By;

import static com.mcshop.sklep.webdriver.common.logging.PageObjectLogger.*;

public class AccountPageObject extends BasePageObject {

    private By aIdentityLocator = By.cssSelector("a#identity-link");
    private By aAdressesLocator = By.cssSelector("a[id^=address]");
    private By aHistoryLocator = By.cssSelector("a#history-link");
    private By aOrderSlipsLocator = By.cssSelector("a#order-slips-link");

    public AddressesPageObject adressesPage(){
        driver.findElement(aAdressesLocator).click();
        log(INFO, "Navigated to addresses pages.");
        return new AddressesPageObject();
    }
}
