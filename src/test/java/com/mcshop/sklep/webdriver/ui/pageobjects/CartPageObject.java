package com.mcshop.sklep.webdriver.ui.pageobjects;

import com.mcshop.sklep.webdriver.ui.BasePageObject;
import org.openqa.selenium.By;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static com.mcshop.sklep.webdriver.common.logging.PageObjectLogger.*;

public class CartPageObject extends BasePageObject {

    private By buttonCheckOutOrder = By.cssSelector("a.btn-primary");
    private By liCartItem = By.cssSelector("li.cart-item");
    private By aRemoveItem = By.cssSelector("a.remove-from-cart");
    private By buttonIncreaseQty = By.cssSelector("button.js-increase-product-quantity");
    private By buttonDecreaseQty = By.cssSelector("button.js-decrease-product-quantity");
    private By spanProductPrice = By.cssSelector("span.product-price");
    private By inputProductQty = By.cssSelector("input.js-cart-line-product-quantity");

    public CartPageObject(){
        super();
    }

    public OrderPageObject checkoutOrder(){
        driver.findElement(buttonCheckOutOrder).click();
        log(INFO, "Checked out order");
        return new OrderPageObject();
    }

    public int getItemsNumber(){
        return driver.findElements(liCartItem).size();
    }

    public CartPageObject removeFirstItem(){
        int itemsNumber = getItemsNumber();
        driver.findElement(aRemoveItem).click();
        waitFor.forNumberElementsToBe(liCartItem, itemsNumber-1);
        log(INFO, "Item has been removed");
        return this;
    }

    public int getFirstProductQty(){
        String textQty = driver.findElement(inputProductQty).getAttribute("value");
        return Integer.parseInt(textQty);
    }

    public CartPageObject increaseFirstProductQty(){
        int productQty = Integer.parseInt(driver.findElement(inputProductQty).getAttribute("value"));
        BigDecimal baseProductPrice = getBigDecimalFromString(driver.findElement(spanProductPrice).getText());
        BigDecimal singleProductPrice = baseProductPrice.divide(new BigDecimal(productQty), RoundingMode.HALF_EVEN);
        driver.findElement(buttonIncreaseQty).click();
        waitFor.forContainingText(spanProductPrice, baseProductPrice.add(singleProductPrice).toString());
        log(INFO, "Product quantity has been increased");
        return this;
    }

    public CartPageObject decreaseFirstProductQty(){
        int productQty = Integer.parseInt(driver.findElement(inputProductQty).getAttribute("value"));
        BigDecimal baseProductPrice = getBigDecimalFromString(driver.findElement(spanProductPrice).getText());
        BigDecimal singleProductPrice = baseProductPrice.divide(new BigDecimal(productQty), RoundingMode.HALF_EVEN);
        driver.findElement(buttonDecreaseQty).click();
        BigDecimal expectedPrice = baseProductPrice.compareTo(singleProductPrice) == 0 ? singleProductPrice : baseProductPrice.subtract(singleProductPrice);
        waitFor.forContainingText(spanProductPrice, expectedPrice.toString());
        log(INFO, "Product quantity has been decreased");
        return this;
    }


}
