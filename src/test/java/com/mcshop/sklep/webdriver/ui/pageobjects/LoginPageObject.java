package com.mcshop.sklep.webdriver.ui.pageobjects;

import com.mcshop.sklep.webdriver.ui.BasePageObject;
import com.mcshop.sklep.webdriver.ui.components.LoginForm;
import org.openqa.selenium.By;

public class LoginPageObject extends BasePageObject {

    private By loginFormLocator = By.cssSelector("form#login-form");
    private By aCreateAccount = By.cssSelector("div.no-account a");

    public LoginPageObject() {
        super();
    }

    public LoginForm loginForm(){
        return new LoginForm(driver.findElement(loginFormLocator));
    }

    public CreateAccountPageObject navigateToCreateAccountForm(){
        driver.findElement(aCreateAccount).click();
        return new CreateAccountPageObject();
    }

}
