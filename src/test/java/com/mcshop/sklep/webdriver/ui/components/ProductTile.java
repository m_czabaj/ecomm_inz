package com.mcshop.sklep.webdriver.ui.components;

import com.mcshop.sklep.webdriver.ui.pageobjects.ProductPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.math.BigDecimal;

import static com.mcshop.sklep.webdriver.common.logging.PageObjectLogger.*;

public class ProductTile extends BaseComponenet{

    private By quickView = By.cssSelector("a.quick-view");
    private By productTitle = By.cssSelector("h2.product-title a");
    private By productDiscount = By.cssSelector("span.discount-percentage");
    private By currentPrice = By.cssSelector("span.price");
    private By regularPrice = By.cssSelector("span.regular-price");

    public ProductTile(WebElement locator){
        super(locator);
    }

    private BigDecimal extractNummber(String str){
        String stringWithComma = str.replaceAll("(?!\\d|,).","");
        return new BigDecimal(stringWithComma.replace(",", "."));
    }

    public BigDecimal getCurrentPrice(){
        String rawPrice = componentLocator.findElement(currentPrice).getText();
        return extractNummber(rawPrice);
    }

    public BigDecimal getRegularPrice(){
        String rawPrice = componentLocator.findElement(regularPrice).getText();
        return extractNummber(rawPrice);
    }

    public ProductPageObject openProductPage(){
        componentLocator.click();
        log(INFO, "Navigated to product page");
        return new ProductPageObject();
    }

    public String getTitle(){
        return componentLocator.findElement(productTitle).getText();
    }


}
