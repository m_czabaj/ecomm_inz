package com.mcshop.sklep.webdriver.ui.pageobjects;

import com.mcshop.sklep.webdriver.ui.BasePageObject;
import com.mcshop.sklep.webdriver.ui.components.ProductTile;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class HomePageObject extends BasePageObject {

    private String productTileBaseXpath = "//a[normalize-space()='%s']/../../../..";
    private By aProductMiniature = By.cssSelector("article.product-miniature");

    public HomePageObject(){
        super();
    }

    public ProductTile findProduct(String name){
        String productXpath = String.format(productTileBaseXpath, name);
        WebElement tile = driver.findElement(By.xpath(productXpath));

        return new ProductTile(tile);
    }

    public ProductTile getFistProduct(){
        WebElement tile = driver.findElement(aProductMiniature);
        return new ProductTile(tile);
    }


}
