package com.mcshop.sklep.webdriver.ui.pageobjects;

import com.mcshop.sklep.webdriver.ui.BasePageObject;
import com.mcshop.sklep.webdriver.ui.components.BlockCartModal;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.mcshop.sklep.webdriver.common.logging.PageObjectLogger.*;

public class ProductPageObject extends BasePageObject {

    private By buttonAddToCart = By.cssSelector("button.add-to-cart");
    private By modalBlockcart = By.cssSelector("div#blockcart-modal");

    public ProductPageObject(){
        super();
    }

    public BlockCartModal addToCart(){
        driver.findElement(buttonAddToCart).click();
        waitFor.forElementVisible(modalBlockcart);
        WebElement modal = driver.findElement(modalBlockcart);
        log(INFO, "Product has been added to cart");
        return new BlockCartModal(modal);
    }
}
