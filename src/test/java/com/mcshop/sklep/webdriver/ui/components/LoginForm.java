package com.mcshop.sklep.webdriver.ui.components;

import com.mcshop.sklep.webdriver.ui.pageobjects.AccountPageObject;
import com.mcshop.sklep.webdriver.ui.pageobjects.RemindPasswordPageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.mcshop.sklep.webdriver.common.logging.PageObjectLogger.*;

public class LoginForm extends BaseComponenet{
    
    private By inputEmail = By.cssSelector("input[name=email]");
    private By inputPassword = By.cssSelector("input[name=password]");
    private By buttonShowHidePasssword = By.cssSelector("button[data-action=show-password]");
    private By aForgotPassword = By.cssSelector("div.forgot-password a");
    private By buttonSubmit = By.cssSelector("button[type=submit]");

    public LoginForm(WebElement locator) {
        super(locator);
    }

    public LoginForm fillEmail(String email){
        componentLocator.findElement(inputEmail).sendKeys(email);
        log(INFO, "Filled email input with string: " + email);
        return this;
    }

    public LoginForm fillPassword(String password){
        componentLocator.findElement(inputPassword).sendKeys(password);
        log(INFO, "Filled email password with string: " + password);
        return this;
    }

    public LoginForm showPassword() {
        String type = componentLocator.findElement(inputPassword).getAttribute("type");
        if(type.equals("password")){
            componentLocator.findElement(buttonShowHidePasssword).click();
        }
        log(INFO, "Made password visible");
        return this;
    }

    public RemindPasswordPageObject remindPassword(){
        componentLocator.findElement(aForgotPassword).click();
        log(INFO, "Navigated to password reminder page");
        return new RemindPasswordPageObject();
    }

    public AccountPageObject submitForm(){
        componentLocator.findElement(buttonSubmit).click();
        log(INFO, "Submitted login form");
        return new AccountPageObject();
    }

    public AccountPageObject loginUser(String email, String password) {
        return fillEmail(email)
                .fillPassword(password)
                .submitForm();
    }


}
