package com.mcshop.sklep.webdriver.ui.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.mcshop.sklep.webdriver.common.logging.PageObjectLogger.*;

public class DeliveryMethod extends BaseComponenet{

    private By buttonConfirmDelivery = By.cssSelector("button[name='confirmDeliveryOption']");
    private By radioPickUpFromStore = By.cssSelector("label[for=delivery_option_1]");
    private By radioCourierDelivery = By.cssSelector("label[for=delivery_option_2]");

    public DeliveryMethod(WebElement elementLocator){
        super(elementLocator);
        wait.forElementVisible(elementLocator, 2);
    }

    public DeliveryMethod pickUpFromStore(){
        componentLocator.findElement(radioPickUpFromStore).click();
        log(INFO, "Chosen delivery method: pick up from store.");
        return this;
    }

    public DeliveryMethod courierDelivery(){
        componentLocator.findElement(radioCourierDelivery).click();
        log(INFO, "Chosen delivery method: courier");
        return this;
    }

    public void confirmDelivery(){
        componentLocator.findElement(buttonConfirmDelivery).click();
        log(INFO, "Delivery method confirmed.");
    }

}
