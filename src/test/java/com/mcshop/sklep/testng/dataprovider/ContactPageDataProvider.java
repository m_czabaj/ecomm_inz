package com.mcshop.sklep.testng.dataprovider;

import org.testng.annotations.DataProvider;

public class ContactPageDataProvider {

    @DataProvider
    public static Object[][] contactFormData(){
        return new Object[][]{
                {"mariusz.czabaj@gmail.com", "Just simple test message", "Your message has been successfully sent to our team."},
                {"mariusz.czabaj@gmail", "Just simple test message", "Invalid email address."}
        };
    }
}
