package com.mcshop.sklep.testng.dataprovider;

import com.mcshop.sklep.helpers.AddressDataHelper;
import org.testng.annotations.DataProvider;

public class AddressesDataProvider {

    @DataProvider
    public static Object[][] newAddressesData(){
        AddressDataHelper addressData = new AddressDataHelper();
        addressData.firstName = "Mariusz";
        addressData.lastName = "Test";
        addressData.address = "Jerozolimskie";
        addressData.city = "Warszawa";
        addressData.postalCode = "00-000";

        return new Object[][]{
                {addressData}
        };
    }
}
