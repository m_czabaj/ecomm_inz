package com.mcshop.sklep.testng.dataprovider;

import com.mcshop.sklep.helpers.AddressDataHelper;
import org.testng.annotations.DataProvider;

public class RegistrationDataProvider {

    @DataProvider
    public static Object[][] newUserData(){
        AddressDataHelper addressData = new AddressDataHelper();
        addressData.firstName = "Automated";
        addressData.lastName = "Test";
        addressData.email = System.currentTimeMillis() +"@automated.test.io";
        addressData.password = "123456";
        addressData.birthdate = "01/01/2000";

        return new Object[][]{
                {addressData}
        };
    }
}
