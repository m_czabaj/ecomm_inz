package com.mcshop.sklep.testng.dataprovider;

import com.mcshop.sklep.helpers.AddressDataHelper;
import org.testng.annotations.DataProvider;

public class OrderDataProdiver {

    @DataProvider
    public static Object[][] orderDataForLoggedUser(){
        return new Object[][]{
                {getTestAddress(), "Hummingbird printed t-shirt", "YOUR ORDER IS CONFIRMED"}
        };
    }

    @DataProvider
    public static Object[][] orderDataForGuest(){
        AddressDataHelper addressData = getTestAddress();
        return new Object[][]{
                {"Mug The best is yet to come", "YOUR ORDER IS CONFIRMED", addressData}
        };
    }

    private static AddressDataHelper getTestAddress(){
        AddressDataHelper addressData = new AddressDataHelper();
        addressData.firstName = "Mariusz";
        addressData.lastName = "Test";
        addressData.email = "mariusz.czabaj+1@gmail.com";
        addressData.address = "Jerozolimskie 67";
        addressData.city = "Warsaw";
        addressData.postalCode = "00-000";

        return addressData;
    }

}
