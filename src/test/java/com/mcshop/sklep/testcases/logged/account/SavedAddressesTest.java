package com.mcshop.sklep.testcases.logged.account;

import com.mcshop.sklep.helpers.AddressDataHelper;
import com.mcshop.sklep.testcases.BaseTest;
import com.mcshop.sklep.testng.dataprovider.AddressesDataProvider;
import com.mcshop.sklep.webdriver.ui.pageobjects.AddressesPageObject;
import com.mcshop.sklep.webdriver.ui.pageobjects.HomePageObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class SavedAddressesTest extends BaseTest {

    private HomePageObject homePage;

    @BeforeClass
    public void setUp(){
        super.loginUser();
        homePage = new HomePageObject();
    }

    @BeforeMethod
    public void navigateToHomePage(){
        homePage.header().clickLogo();
    }

    @Test(
            dataProviderClass = AddressesDataProvider.class,
            dataProvider = "newAddressesData"
    )
    public void addNewAddress(AddressDataHelper addressData){
        AddressesPageObject addressesPage = homePage.header().navigateToAccount().adressesPage();
        int baseAddressesNumber = addressesPage.addressForm().savedAddressesList().size();
        addressesPage.addressForm().addNewAddress().fillEntireAddressIfNoneExists(addressData).saveForm();
        int afterAddressesNumber = addressesPage.addressForm().savedAddressesList().size();
        assertEquals(afterAddressesNumber, baseAddressesNumber + 1, "Addresses number was ");
    }

    @Test
    public void removingSavedAddresses(){
        AddressesPageObject addresses = homePage.header().navigateToAccount().adressesPage();
        int baseAddressesNumber = addresses.addressForm().savedAddressesList().size();
        addresses.addressForm().savedAddressesList().get(0).deleteAddress();
        int afterAddressesNumber = addresses.addressForm().savedAddressesList().size();
        assertEquals(afterAddressesNumber, baseAddressesNumber - 1, "Addresses number was ");
    }

    @AfterClass
    public void tearDown(){
        logoutUser();
    }
}
