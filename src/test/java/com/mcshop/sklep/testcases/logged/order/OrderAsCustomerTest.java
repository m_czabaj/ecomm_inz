package com.mcshop.sklep.testcases.logged.order;

import com.mcshop.sklep.helpers.AddressDataHelper;
import com.mcshop.sklep.testcases.BaseTest;
import com.mcshop.sklep.testng.dataprovider.OrderDataProdiver;
import com.mcshop.sklep.webdriver.ui.pageobjects.HomePageObject;
import com.mcshop.sklep.webdriver.ui.pageobjects.OrderPageObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class OrderAsCustomerTest extends BaseTest {

    private HomePageObject homePage;

    @BeforeClass
    public void loginUser(){
        homePage = new HomePageObject();
        super.loginUser();
    }

    @BeforeMethod
    public void navigateToHomePage(){
        homePage.header().clickLogo();
    }

    @Test(
            dataProviderClass = OrderDataProdiver.class,
            dataProvider = "orderDataForLoggedUser"
    )
    public void makeOrder(AddressDataHelper customerData,String productName, String expectedTitle){
        OrderPageObject orderPage = homePage.findProduct(productName)
                .openProductPage()
                .addToCart()
                .goToCart()
                .checkoutOrder();

        orderPage.addressForm().fillEntireAddressIfNoneExists(customerData).saveForm();
        orderPage.deliveryMethod().pickUpFromStore().confirmDelivery();
        String pageTitle = orderPage.paymentMethod().makeBankTransfer().checkAgreement().confirmPayment().getConfirmationText();

        assertEquals(pageTitle, expectedTitle);
    }

    @AfterClass
    public void tearDown(){
        logoutUser();
    }

}
