package com.mcshop.sklep.testcases;

import static com.mcshop.sklep.webdriver.common.configuration.Configuration.HOST;
import static com.mcshop.sklep.webdriver.core.driverprovider.DriverProvider.*;

import com.mcshop.sklep.webdriver.common.listener.Listener;
import com.mcshop.sklep.webdriver.core.browserdriver.BrowserDriver;
import com.mcshop.sklep.webdriver.ui.pageobjects.HomePageObject;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;

@Listeners(Listener.class)
public class BaseTest {

    protected BrowserDriver driver;

    public BaseTest(){
        driver = getActiveDriver();
    }

    protected void loginUser(){
        new HomePageObject().header()
                .navigateToLoginPage()
                .loginForm()
                .loginUser("mariusz.czabaj+test@gmail.com", "12345678");
    }

    protected void logoutUser(){
        new HomePageObject().header().logOut();
    }

    @BeforeSuite
    public void startDriver(){
        getActiveDriver();
        driver.navigate().to(HOST);
    }

    @AfterSuite
    public void closeBrowser() {
        close();
    }
}
