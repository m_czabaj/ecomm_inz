package com.mcshop.sklep.testcases.guest.registration;

import com.mcshop.sklep.helpers.AddressDataHelper;
import com.mcshop.sklep.testcases.BaseTest;
import com.mcshop.sklep.testng.dataprovider.RegistrationDataProvider;
import com.mcshop.sklep.webdriver.ui.pageobjects.CreateAccountPageObject;
import com.mcshop.sklep.webdriver.ui.pageobjects.HomePageObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class RegistrationTest extends BaseTest {

    @Test(
            dataProviderClass = RegistrationDataProvider.class,
            dataProvider = "newUserData"
    )
    public void registerUser(AddressDataHelper userData){
        HomePageObject homePage = new HomePageObject();
        CreateAccountPageObject registerPage = homePage.header()
                .navigateToLoginPage()
                .navigateToCreateAccountForm();

        homePage = registerPage.registerForm().fillAndSendEntireFormWith(userData);

        String loggedUserData = homePage.header().getLoggedUserData();

        Assert.assertTrue(loggedUserData.contains(userData.firstName));
    }

    @AfterClass
    public void logOut(){
        new HomePageObject().header().logOut();
    }
}
