package com.mcshop.sklep.testcases.guest.cart;

import com.mcshop.sklep.testcases.BaseTest;
import com.mcshop.sklep.webdriver.ui.pageobjects.CartPageObject;
import com.mcshop.sklep.webdriver.ui.pageobjects.HomePageObject;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CartTest extends BaseTest {

    private HomePageObject homePage;

    @BeforeMethod
    public void navigateToHomePage(){
        homePage = new HomePageObject();
        homePage.header().clickLogo();
    }

    @Test
    public void increaseItemQuantityInCart(){
        CartPageObject cart = homePage.getFistProduct()
                .openProductPage()
                .addToCart()
                .goToCart();

        int baseItemQty = cart.getFirstProductQty();
        cart.increaseFirstProductQty();
        int afterItemQty = cart.getFirstProductQty();

        assertEquals(afterItemQty, baseItemQty+1);
    }

    @Test
    public void decreaseItemQuantityInCart(){
        CartPageObject cart = homePage.getFistProduct()
                .openProductPage()
                .addToCart()
                .goToCart();

        int baseItemQty = cart.getFirstProductQty();
        cart.decreaseFirstProductQty();
        int afterItemQty = cart.getFirstProductQty();

        assertEquals(afterItemQty, baseItemQty);
    }

    @Test
    public void removeItemFromCart(){
        CartPageObject cart = homePage.getFistProduct()
                .openProductPage()
                .addToCart()
                .goToCart();

        int baseItemsNumber = cart.getItemsNumber();
        cart.removeFirstItem();
        int afterItemsNumber = cart.getItemsNumber();

        assertEquals(afterItemsNumber, baseItemsNumber-1);
    }
}
