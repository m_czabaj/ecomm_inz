package com.mcshop.sklep.testcases.guest.products;

import com.mcshop.sklep.testcases.BaseTest;
import com.mcshop.sklep.webdriver.ui.components.ProductTile;
import com.mcshop.sklep.webdriver.ui.pageobjects.HomePageObject;
import com.mcshop.sklep.webdriver.ui.pageobjects.SearchPageObject;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;

public class SearchTest extends BaseTest {

    @Test
    public void searchProduct(){
        SearchPageObject searchPage = new HomePageObject()
                .header()
                .searchProduct("mug");

        List<ProductTile> products = searchPage.productSection().getAllProducts();
        assertTrue(products.size() > 0, "No products found but should be at least 1.");
        assertTrue(products.stream().allMatch(p -> p.getTitle().contains("Mug")), "Not all products titles contain word: Mug");
    }
}
