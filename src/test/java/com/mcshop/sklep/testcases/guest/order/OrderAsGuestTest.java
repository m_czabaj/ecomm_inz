package com.mcshop.sklep.testcases.guest.order;

import com.mcshop.sklep.helpers.AddressDataHelper;
import com.mcshop.sklep.testcases.BaseTest;
import com.mcshop.sklep.testng.dataprovider.OrderDataProdiver;
import com.mcshop.sklep.webdriver.ui.pageobjects.HomePageObject;
import com.mcshop.sklep.webdriver.ui.pageobjects.OrderPageObject;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class OrderAsGuestTest extends BaseTest {

    private HomePageObject homePage;

    @BeforeMethod
    public void navigateToHomePage(){
        homePage = new HomePageObject();
        homePage.header().clickLogo();
    }

    @Test(
            dataProviderClass = OrderDataProdiver.class,
            dataProvider = "orderDataForGuest"
    )
    public void makeOrderAsGuest(String productName, String expectedTitle, AddressDataHelper addressData){
        OrderPageObject orderPage = homePage.findProduct(productName)
                .openProductPage()
                .addToCart()
                .goToCart()
                .checkoutOrder();

        orderPage.personalInformationForm().fillPersonalInformation(addressData).saveForm();
        orderPage.addressForm().fillEntireAddress(addressData).saveForm();
        orderPage.deliveryMethod().pickUpFromStore().confirmDelivery();
        String pageTitle = orderPage.paymentMethod().makeBankTransfer().checkAgreement().confirmPayment().getConfirmationText();

        assertEquals(pageTitle, expectedTitle);
    }
}
