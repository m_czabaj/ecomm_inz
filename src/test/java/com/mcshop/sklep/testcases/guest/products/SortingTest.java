package com.mcshop.sklep.testcases.guest.products;

import com.google.common.collect.Ordering;
import com.mcshop.sklep.testcases.BaseTest;
import com.mcshop.sklep.webdriver.ui.components.ProductTile;
import com.mcshop.sklep.webdriver.ui.pageobjects.CategoryPageObject;
import com.mcshop.sklep.webdriver.ui.pageobjects.HomePageObject;
import org.testng.annotations.Test;

import java.util.List;

import static java.util.stream.Collectors.*;
import static org.testng.Assert.*;

public class SortingTest extends BaseTest {

    @Test
    public void checkAlfabeticalSort(){
        CategoryPageObject categoryPage = new HomePageObject()
                .header()
                .openCategory("Accessories");

        categoryPage.productsSection().changeSortingTo("Name, A to Z");
        List<String> titles = categoryPage.productsSection()
                .getAllProducts()
                .stream()
                .map(ProductTile::getTitle)
                .collect(toList());

        assertTrue(Ordering.natural().isOrdered(titles), "Products are not sorted in alfabetical order.");
    }
}
