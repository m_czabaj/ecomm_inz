package com.mcshop.sklep.testcases.guest.products;

import com.mcshop.sklep.testcases.BaseTest;
import com.mcshop.sklep.webdriver.ui.pageobjects.CategoryPageObject;
import com.mcshop.sklep.webdriver.ui.pageobjects.HomePageObject;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FiltersTest extends BaseTest {

    @Test
    public void filterMenClothes(){
        CategoryPageObject categoryPage = new HomePageObject()
                .header()
                .openCategory("Clothes");

        categoryPage.filters().selectFacet("Men");
        int products = categoryPage.productsSection().getAllProducts().size();

        Assert.assertTrue(products > 0, "Numbers of men clothes should be more than 0.");
    }
}
