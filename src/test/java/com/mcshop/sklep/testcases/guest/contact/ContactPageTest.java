package com.mcshop.sklep.testcases.guest.contact;

import com.mcshop.sklep.testcases.BaseTest;
import com.mcshop.sklep.testng.dataprovider.ContactPageDataProvider;
import com.mcshop.sklep.webdriver.ui.pageobjects.ContactPageObject;
import com.mcshop.sklep.webdriver.ui.pageobjects.HomePageObject;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ContactPageTest extends BaseTest {

    @Test(
            dataProviderClass = ContactPageDataProvider.class,
            dataProvider = "contactFormData"
    )
    public void successfulSendingContactForm(String email, String messageToSend, String confirmationMesage){
        ContactPageObject contactPage = new HomePageObject().header().navigateToContactForm();
        contactPage.selectSubjectByIndex(1)
                .fillEmail(email)
                .fillMessage(messageToSend)
                .sendForm();

        String message = contactPage.getMessage();

        assertEquals(message, confirmationMesage);
    }
}
